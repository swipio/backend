from typing import Dict

import pytest
from sqlalchemy.orm import Session

from swipio.db.exceptions import NotFoundRepoException
from swipio.db.repositories import ParamsRepository


@pytest.fixture(name='empty_params_repo')
def _empty_params_repo(db_session: Session) -> ParamsRepository:
    return ParamsRepository(db_session)


@pytest.fixture(name='params')
def _params() -> Dict[str, str]:
    return {
        'param1': 'some value 1',
        'version': '0.0.0.0.0.01.234.4',
        'more param': 'Some strange',
        'unicode': '📝⚡️Start',
    }


@pytest.fixture(name='fake_params_repo')
def _fake_params_repo(
    empty_params_repo: ParamsRepository,
    params: Dict[str, str],
) -> ParamsRepository:
    for k, v in params.items():
        empty_params_repo.set(k, v)
    return empty_params_repo


def test_error_get_params(
    empty_params_repo: ParamsRepository,
    params: Dict[str, str],
) -> None:
    for k in params.keys():
        with pytest.raises(NotFoundRepoException):
            empty_params_repo.get(k)


def test_get_all_params(
    fake_params_repo: ParamsRepository,
    params: Dict[str, str],
) -> None:
    for k, v in params.items():
        assert fake_params_repo.get(k) == v


def test_param_reassign(
    fake_params_repo: ParamsRepository,
) -> None:
    fake_params_repo.set('param1', 'value1')
    assert fake_params_repo.get('param1') == 'value1'
    fake_params_repo.set('param1', 'value2')
    assert fake_params_repo.get('param1') == 'value2'

from typing import List, Tuple

import pytest
from sqlalchemy.orm import Session

from swipio.db.exceptions import NotFoundRepoException
from swipio.db.repositories import FilmsRepository
from swipio.models.films import Film


@pytest.fixture(name='empty_films_repo')
def _empty_films_repo(db_session: Session) -> FilmsRepository:
    return FilmsRepository(db_session)


@pytest.fixture(name='films')
def _films() -> List[Tuple[Film, bool]]:
    return [
        (
            Film(
                film_id=1,
                title='Терминатор 2: Судный день 3D',
                description='Прошло более десяти лет с тех пор, как киборг-терминатор...',
                release_year=2017,
                duration=100,
                preview_image_url='some_link',
            ),
            True,
        ),
        (
            Film(
                film_id=2,
                title='Гнев человеческий',
                description='Эйч — загадочный и холодный на вид джентльмен, но внутри...',
                release_year=2021,
                duration=100,
                preview_image_url='some_link',
            ),
            False,
        ),
        (
            Film(
                film_id=3,
                title='Унесенные призраками',
                description='Тихиро с мамой и папой переезжают в новый дом. Заблудившись...',
                release_year=2001,
                duration=100,
                preview_image_url='some_link',
            ),
            False,
        ),
        (
            Film(
                film_id=4,
                title='Законопослушный гражданин',
                description='У примерного семьянина Клайда Шелтона преступники убивают...',
                release_year=2009,
                duration=100,
                preview_image_url='some_link',
            ),
            True,
        ),
        (
            Film(
                film_id=5,
                title='12 стульев',
                description='В уездном городе N жизнь была тишайшая. Но в пятницу 15 апреля...',
                release_year=1976,
                duration=100,
                preview_image_url='some_link',
            ),
            True,
        ),
    ]


@pytest.fixture(name='fake_films_repo')
def _fake_films_repo(
    empty_films_repo: FilmsRepository, films: List[Tuple[Film, bool]]
) -> FilmsRepository:
    for film, is_active in films:
        film_id = empty_films_repo.add_new_film(
            film.title,
            film.description,
            film.release_year,
            film.duration,
            film.preview_image_url,
        )
        assert film.film_id == film_id
        if not is_active:
            empty_films_repo.remove_film(film_id)
    return empty_films_repo


def test_get_film_by_id(
    fake_films_repo: FilmsRepository, films: List[Tuple[Film, bool]]
) -> None:
    for film, is_active in films:
        if is_active:
            res = fake_films_repo.get_film_by_id(film.film_id)
            assert not res.removed
            assert Film(**res.dict()) == film
        else:
            with pytest.raises(NotFoundRepoException):
                fake_films_repo.get_film_by_id(film.film_id)


@pytest.mark.parametrize(
    'offset,limit',
    [
        (1, 8),
        (0, 1),
    ],
)
def test_get_films(
    fake_films_repo: FilmsRepository,
    films: List[Tuple[Film, bool]],
    offset: int,
    limit: int,
) -> None:
    res = fake_films_repo.get_films(offset, limit)

    target_films = [f[0] for f in films if f[1]][offset : offset + limit]

    assert len(res) == len(target_films)
    for r, t in zip(res, target_films):
        assert Film(**r.dict()) == t


@pytest.mark.parametrize(
    'films_id',
    [
        [1, 2, 3, 4, 5],
        [1],
    ],
)
def test_get_films_by_id(
    fake_films_repo: FilmsRepository,
    films: List[Tuple[Film, bool]],
    films_id: List[int],
) -> None:
    res = fake_films_repo.get_films_by_id(films_id)

    # only active films
    target_films = [f[0] for f in films if f[1] and f[0].film_id in films_id]

    assert len(res) == len(target_films)
    for r, t in zip(res, target_films):
        assert Film(**r.dict()) == t

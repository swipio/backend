import os

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from swipio.db.models import SQLBase


@pytest.fixture(name='db_session_maker')
def _db_session_maker() -> sessionmaker:
    if os.path.isfile('test.db'):
        os.remove('test.db')

    engine = create_engine(
        'sqlite:///test.db', connect_args={'check_same_thread': False}
    )
    SQLBase.metadata.create_all(bind=engine)

    yield sessionmaker(autocommit=False, autoflush=False, bind=engine)

    os.remove('test.db')


@pytest.fixture(name='db_session')
def _db_session(db_session_maker: sessionmaker) -> Session:
    new_session = db_session_maker()
    yield new_session
    new_session.close()

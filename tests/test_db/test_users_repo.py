from typing import Any, Dict, List

import pytest
from sqlalchemy.orm import Session

from swipio.db.exceptions import AlreadyExistRepoException, NotFoundRepoException
from swipio.db.repositories import UsersRepository


@pytest.fixture(name='empty_users_repo')
def _empty_users_repo(db_session: Session) -> UsersRepository:
    return UsersRepository(db_session)


@pytest.fixture(name='users')
def _users() -> List[Dict[str, Any]]:
    return [
        {
            'login': 'testUser',
            'hashed_password': 'sef wef ',
            'first_name': 'user1',
            'last_name': 'uuu',
            'disabled': False,
        },
        {
            'login': 'testUser2',
            'hashed_password': '3uhoco7 O*4fv3 4',
            'first_name': 'user2',
            'last_name': 'uuu',
            'disabled': True,
        },
        {
            'login': 'f',
            'hashed_password': '2d2d2dlhbO*(Y',
            'first_name': 'user3',
            'last_name': 'uuu',
            'disabled': False,
        },
    ]


@pytest.fixture(name='fake_users_repo')
def _fake_users_repo(
    empty_users_repo: UsersRepository,
    users: List[Dict[str, Any]],
) -> UsersRepository:
    for i, user in enumerate(users):
        user_id = empty_users_repo.create_user(**user)
        assert user_id == i + 1
    return empty_users_repo


def test_users_with_same_login(
    empty_users_repo: UsersRepository, users: List[Dict[str, Any]]
) -> None:
    empty_users_repo.create_user(**users[0])

    fake_user = users[1].copy()
    fake_user['login'] = users[0]['login']  # same login

    with pytest.raises(AlreadyExistRepoException):
        empty_users_repo.create_user(**fake_user)


def test_get_user_login_data(
    fake_users_repo: UsersRepository, users: List[Dict[str, Any]]
) -> None:
    for user in users:
        res = fake_users_repo.get_user_login_data(user['login'])
        assert res.login == user['login']
        assert res.hashed_password == user['hashed_password']
        assert res.disabled == user['disabled']


def test_error_user_login_data(fake_users_repo: UsersRepository) -> None:
    with pytest.raises(NotFoundRepoException):
        fake_users_repo.get_user_login_data('some unexisted login')


def test_get_user_by_id(
    fake_users_repo: UsersRepository, users: List[Dict[str, Any]]
) -> None:
    for i, user in enumerate(users):
        res = fake_users_repo.get_user_by_id(i + 1)
        assert res.user_id == i + 1
        assert res.login == user['login']
        assert res.first_name == user['first_name']
        assert res.last_name == user['last_name']
        assert res.disabled == user['disabled']


def test_error_user_by_id(
    fake_users_repo: UsersRepository,
) -> None:
    with pytest.raises(NotFoundRepoException):
        fake_users_repo.get_user_by_id(6)


def test_get_user_perm_by_id(
    fake_users_repo: UsersRepository, users: List[Dict[str, Any]]
) -> None:
    for i, user in enumerate(users):
        res = fake_users_repo.get_user_perm_by_id(i + 1)
        assert res.user_id == i + 1
        assert res.disabled == user['disabled']
        assert not res.is_admin


def test_error_user_perm_by_id(fake_users_repo: UsersRepository) -> None:
    with pytest.raises(NotFoundRepoException):
        fake_users_repo.get_user_perm_by_id(9)

from typing import List, Tuple

import pytest
from sqlalchemy.orm import Session

from swipio.db.repositories import FilmMarksRepository
from swipio.models.film_marks import FilmMarkType


@pytest.fixture(name='empty_film_mark_repo')
def _empty_film_mark_repo(db_session: Session) -> FilmMarksRepository:
    return FilmMarksRepository(db_session)


@pytest.fixture(name='film_marks')
def _film_marks() -> List[Tuple[int, int, FilmMarkType]]:
    return [
        (1, 1, FilmMarkType.WANT_TO_WATCH),
        (1, 5, FilmMarkType.DONT_WANT_TO_WATCH),
        (1, 6, FilmMarkType.WANT_TO_WATCH),
        (8, 1, FilmMarkType.WANT_TO_WATCH),
        (8, 2, FilmMarkType.DONT_WANT_TO_WATCH),
    ]


@pytest.fixture(name='fake_film_mark_repo')
def _fake_film_mark_repo(
    empty_film_mark_repo: FilmMarksRepository,
    film_marks: List[Tuple[int, int, FilmMarkType]],
) -> FilmMarksRepository:
    for user_id, film_id, mark in film_marks:
        empty_film_mark_repo.edit_mark(user_id, film_id, mark)
    return empty_film_mark_repo


@pytest.mark.parametrize(
    'user_id,film_id',
    [
        (0, 0),
        (0, 1),
        (24, 8723),
        (-18782, -1827),
    ],
)
def test_get_unknown_mark(
    empty_film_mark_repo: FilmMarksRepository, user_id: int, film_id: int
) -> None:
    res = empty_film_mark_repo.get_mark(user_id, film_id)
    assert res.user_id == user_id
    assert res.film_id == film_id
    assert res.mark == FilmMarkType.UNKNOWN


def test_get_all_mark(
    fake_film_mark_repo: FilmMarksRepository,
    film_marks: List[Tuple[int, int, FilmMarkType]],
) -> None:
    for user_id, film_id, mark in film_marks:
        res = fake_film_mark_repo.get_mark(user_id, film_id)
        assert res.mark == mark

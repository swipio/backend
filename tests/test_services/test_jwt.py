import secrets
import time
from datetime import timedelta
from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture

from swipio.db.exceptions import NotFoundRepoException
from swipio.services import JWTService
from swipio.services.jwt import BadTokenJWTServiceException


@pytest.fixture(name='fake_params_repo')
def _fake_params_repo(mocker: MockerFixture) -> MagicMock:
    res = mocker.MagicMock()
    res.get.return_value = secrets.token_hex(32)
    return res


@pytest.fixture(name='empty_params_repo')
def _empty_params_repo(mocker: MockerFixture) -> MagicMock:
    res = mocker.MagicMock()
    res.get.side_effect = NotFoundRepoException()
    return res


@pytest.fixture(name='jwt_service')
def _jwt_service() -> JWTService:
    secret_key = secrets.token_hex(32)
    return JWTService(secret_key)


@pytest.mark.parametrize('user_id', [0, 5, -6])
def test_correct_token(jwt_service: JWTService, user_id: int) -> None:
    token = jwt_service.create_access_token_for_user(user_id)
    assert user_id == jwt_service.get_user_id_from_token(token)


@pytest.mark.parametrize(
    'token',
    [
        '',
        ' . . ',
        'g.⚡️.📝',
        'let me in, please',
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzY0NjY1MTcsI'
        'nN1YiI6Ii02In0.Bv1EH2Yq2NaYFfE5X79wlobYhRfrYxhH8wnNVR0Dt9k',
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MzY0NjY1MTcsI'
        'nN1YiI6Ii02In0Bv1EH2Yq2NaYFfE5X79wlobYhRfrYxhH8wnNVR0Dk9k',
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJleHAiOjE2MzY0NjY1MTcsIn'
        'N1YiI6Ii02In0.Bv1EH2Yq2NaYFfE5X79wlobYhRfrYxhH8wnNVR0Dk9k',
    ],
)
def test_incorrect_token(jwt_service: JWTService, token: str) -> None:
    with pytest.raises(BadTokenJWTServiceException):
        jwt_service.get_user_id_from_token(token)


def test_expired_token(jwt_service: JWTService) -> None:
    token = jwt_service.create_access_token_for_user(1, timedelta(seconds=1))
    assert jwt_service.get_user_id_from_token(token) == 1
    time.sleep(2.1)
    with pytest.raises(BadTokenJWTServiceException):
        jwt_service.get_user_id_from_token(token)


def test_get_jwt_secret_key(fake_params_repo: MagicMock) -> None:
    # pylint: disable=protected-access
    secret_key = JWTService._get_or_generate_jwt_secret_key(fake_params_repo)
    fake_params_repo.get.assert_called_once_with('jwt_secret_key')
    assert secret_key == fake_params_repo.get('jwt_secret_key')


def test_generate_jwt_secret_key(empty_params_repo: MagicMock) -> None:
    # pylint: disable=protected-access
    secret_key = JWTService._get_or_generate_jwt_secret_key(empty_params_repo)
    assert len(secret_key) == 64

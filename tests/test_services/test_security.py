import pytest

from swipio.services import SecurityService


@pytest.fixture(name='security_service')
def _security_service() -> SecurityService:
    return SecurityService()


@pytest.mark.parametrize(
    'password', ['123456', '1q2w3e', 'ku23bc', '12hjdw', '8881j.c,el k3wef']
)
def test_correct_password(security_service: SecurityService, password: str) -> None:
    hashed_password = security_service.get_password_hash(password)
    assert security_service.verify_password(password, hashed_password)


@pytest.mark.parametrize(
    'password1, password2',
    [('123456', '1234567'), ('1q2w3e', 'ku23bc'), ('12hjdw', '8881j.c,el k3wef')],
)
def test_incorrect_password(
    security_service: SecurityService, password1: str, password2: str
) -> None:
    hashed_password = security_service.get_password_hash(password1)
    assert not security_service.verify_password(password2, hashed_password)

from datetime import datetime

from swipio.models.film_marks import FilmMarkInDB, FilmMarkType

from ..models import FilmMarkDB
from .base import BaseRepository


class FilmMarksRepository(BaseRepository):
    def _int2mark(self, v: int) -> FilmMarkType:
        return [
            FilmMarkType.UNKNOWN,
            FilmMarkType.WANT_TO_WATCH,
            FilmMarkType.DONT_WANT_TO_WATCH,
        ][v]

    def _mark2int(self, v: FilmMarkType) -> int:
        return {
            FilmMarkType.UNKNOWN: 0,
            FilmMarkType.WANT_TO_WATCH: 1,
            FilmMarkType.DONT_WANT_TO_WATCH: 2,
        }[v]

    def get_mark(self, user_id: int, film_id: int) -> FilmMarkInDB:
        """Mark film as removed in the database"""
        res = (
            self._db.query(FilmMarkDB.mark, FilmMarkDB.updated_at)
            .filter_by(user_id=user_id, film_id=film_id)
            .first()
        )
        if not res:
            return FilmMarkInDB(
                user_id=user_id, film_id=film_id, updated_at=datetime.now()
            )
        return FilmMarkInDB(
            user_id=user_id,
            film_id=film_id,
            mark=self._int2mark(res[0]),
            updated_at=res[1],
        )

    def edit_mark(
        self,
        user_id: int,
        film_id: int,
        mark: FilmMarkType,
    ) -> None:
        """Add file to the database"""
        new_film = FilmMarkDB(
            user_id=user_id, film_id=film_id, mark=self._mark2int(mark)
        )
        self._db.merge(new_film)
        self._db.commit()

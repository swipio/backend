from typing import List

from sqlalchemy import and_, not_

from swipio.models.films import FilmInDB

from ..exceptions import NotFoundRepoException
from ..models import FilmDB
from .base import BaseRepository


class FilmsRepository(BaseRepository):
    def add_new_film(
        self,
        title: str,
        description: str,
        release_year: int,
        duration: int,
        preview_image_url: str,
    ) -> int:
        """Add file to the database"""
        new_film = FilmDB(
            title=title,
            description=description,
            release_year=release_year,
            duration=duration,
            preview_image_url=preview_image_url,
        )
        self._db.add(new_film)
        self._db.commit()
        return new_film.film_id

    def remove_film(self, film_id: int) -> None:
        """Mark film as removed in the database"""
        self._db.query(FilmDB).filter_by(film_id=film_id).update({FilmDB.removed: True})
        self._db.commit()

    def get_film_by_id(self, film_id: int) -> FilmInDB:
        res = (
            self._db.query(FilmDB)
            .filter_by(film_id=film_id, removed=False)
            .order_by(FilmDB.film_id)
            .first()
        )
        if not res:
            raise NotFoundRepoException(f"Film with film_id '{film_id}' do not found")
        return FilmInDB.from_orm(res)

    def get_films(self, offset: int, limit: int) -> List[FilmInDB]:
        films_info_raw = (
            self._db.query(FilmDB)
            .filter_by(removed=False)
            .order_by(FilmDB.film_id)
            .offset(offset)
            .limit(limit)
            .all()
        )
        return [FilmInDB.from_orm(f) for f in films_info_raw]

    def get_films_by_id(self, films_id: List[int]) -> List[FilmInDB]:
        films_info_raw = (
            self._db.query(FilmDB)
            .filter(and_(not_(FilmDB.removed), FilmDB.film_id.in_(films_id)))
            .all()
        )
        return [FilmInDB.from_orm(f) for f in films_info_raw]

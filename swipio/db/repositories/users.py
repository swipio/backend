from sqlalchemy.exc import IntegrityError

from swipio.models.users import UserInDB, UserLoginInfoInDB, UserPermInDB

from ..exceptions import AlreadyExistRepoException, NotFoundRepoException
from ..models import UserDB
from .base import BaseRepository


class UsersRepository(BaseRepository):
    def create_user(
        self,
        login: str,
        hashed_password: str,
        first_name: str,
        last_name: str,
        disabled: bool = False,
    ) -> int:
        try:
            new_user = UserDB(
                login=login,
                hashed_password=hashed_password,
                first_name=first_name,
                last_name=last_name,
                disabled=disabled,
            )
            self._db.add(new_user)
            self._db.commit()
            return new_user.user_id
        except IntegrityError as e:
            raise AlreadyExistRepoException(
                f"User with login '{login}' already exist"
            ) from e

    def get_user_login_data(self, login: str) -> UserLoginInfoInDB:
        res = (
            self._db.query(UserDB.user_id, UserDB.hashed_password, UserDB.disabled)
            .filter_by(login=login)
            .first()
        )
        if not res:
            raise NotFoundRepoException(f"User with login '{login}' do not found")
        return UserLoginInfoInDB(
            user_id=res[0], login=login, hashed_password=res[1], disabled=res[2]
        )

    def get_user_by_id(self, user_id: int) -> UserInDB:
        res = self._db.query(UserDB).filter_by(user_id=user_id).first()
        if not res:
            raise NotFoundRepoException(f"User with user_id '{user_id}' do not found")
        return UserInDB.from_orm(res)

    def get_user_perm_by_id(self, user_id: int) -> UserPermInDB:
        res = (
            self._db.query(UserDB.disabled, UserDB.is_admin)
            .filter_by(user_id=user_id)
            .first()
        )
        if not res:
            raise NotFoundRepoException(f"User with user_id '{user_id}' do not found")
        return UserPermInDB(user_id=user_id, disabled=res[0], is_admin=res[1])

from swipio.db.models import ParamsDB

from ..exceptions import NotFoundRepoException
from .base import BaseRepository


class ParamsRepository(BaseRepository):
    def get(self, name: str) -> str:
        print(name)
        res = self._db.query(ParamsDB.value).filter_by(name=name).first()
        if res is None or res[0] is None:
            raise NotFoundRepoException(f"Parameter with name '{name}' do not found")
        return res[0]

    def set(self, name: str, value: str) -> None:
        print(name, value)
        self._db.merge(ParamsDB(name=name, value=value))
        self._db.commit()

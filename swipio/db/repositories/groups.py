from typing import List, Tuple

from sqlalchemy import and_, desc, func, or_
from sqlalchemy.orm import Query, Session

from swipio.models.group import (
    GroupFilmStat,
    GroupFilmWithStat,
    GroupInDB,
    GroupUserStat,
    GroupUserWithStat,
)
from swipio.models.users import UserInDB

from ..exceptions import NotFoundRepoException
from ..models import FilmDB, FilmMarkDB, GroupDB, UserDB, UserGroupDB
from .base import BaseRepository
from .films import FilmsRepository


class GroupsRepository(BaseRepository):

    _films: FilmsRepository

    def __init__(self, db: Session) -> None:
        super().__init__(db)
        self._films = FilmsRepository(db)

    def add_new_group(self, name: str, description: str, creator_id: int) -> int:
        """Add group to the database"""
        new_group = GroupDB(name=name, description=description, creator_id=creator_id)
        self._db.add(new_group)
        self._db.commit()

        self.join_to_group(creator_id, new_group.group_id)

        return new_group.group_id

    def join_to_group(self, user_id: int, group_id: int) -> None:
        new_user_group = UserGroupDB(user_id=user_id, group_id=group_id)
        self._db.merge(new_user_group)
        self._db.commit()

    def is_user_in_group(self, user_id: int, group_id: int) -> bool:
        res = (
            self._db.query(UserGroupDB.created_at)
            .filter_by(user_id=user_id, group_id=group_id)
            .first()
        )
        return res is not None

    def _get_group_users_id(self, group_id: int) -> List[int]:
        res = self._db.query(UserGroupDB.user_id).filter_by(group_id=group_id).all()
        return [u[0] for u in res]

    def _get_group_users_count(self, group_id: int) -> int:
        res = (
            self._db.query(func.count(UserGroupDB.user_id))
            .filter_by(group_id=group_id)
            .first()
        )
        assert res is not None
        return res[0]

    def get_group_by_id(self, group_id: int) -> GroupInDB:
        """Mark film as removed in the database"""
        res = self._db.query(GroupDB).filter_by(group_id=group_id).first()
        if not res:
            raise NotFoundRepoException(
                f"Group with group_id '{group_id}' do not found"
            )

        res_users: List[UserDB] = res.users
        users = [
            GroupUserWithStat(user=UserInDB.from_orm(u), stat=GroupUserStat())
            for u in res_users
        ]

        return GroupInDB(
            group_id=res.group_id,
            name=res.name,
            description=res.description,
            users=users,
            created_at=res.created_at,
            updated_at=res.updated_at,
        )

    def _get_film_stat_subquery(self, group_id: int) -> Query:
        group_users = self._db.query(UserGroupDB.user_id).filter_by(group_id=group_id)

        # query number of marks for earch (film_id, mark) pair
        t = (
            self._db.query(
                FilmMarkDB.film_id, FilmMarkDB.mark, func.count().label('count')
            )
            .filter(FilmMarkDB.user_id.in_(group_users))
            .group_by(FilmMarkDB.film_id, FilmMarkDB.mark)
            .cte('t')  # create temporary table
        )

        t_unknown = (
            self._db.query(t.c.film_id, t.c.count)
            .filter_by(mark=0)
            .subquery('t_unknown')
        )
        t_like = (
            self._db.query(t.c.film_id, t.c.count).filter_by(mark=1).subquery('t_like')
        )
        t_dont_like = (
            self._db.query(t.c.film_id, t.c.count)
            .filter_by(mark=2)
            .subquery('t_dont_like')
        )

        return (
            self._db.query(
                FilmDB.film_id,
                func.coalesce(t_unknown.c.count, 0).label('unknown'),
                func.coalesce(t_like.c.count, 0).label('like'),
                func.coalesce(t_dont_like.c.count, 0).label('dont_like'),
            )
            .select_from(FilmDB)
            .outerjoin(t_unknown, FilmDB.film_id == t_unknown.c.film_id)
            .outerjoin(t_like, FilmDB.film_id == t_like.c.film_id)
            .outerjoin(t_dont_like, FilmDB.film_id == t_dont_like.c.film_id)
            .subquery('film_stat')
        )

    def _get_film_rating_q(self, group_id: int) -> Query:
        film_stat_sq = self._get_film_stat_subquery(group_id)

        rating_func = film_stat_sq.c.like - film_stat_sq.c.dont_like
        watched_func = or_(film_stat_sq.c.like > 0, film_stat_sq.c.dont_like > 0)

        return (
            self._db.query(
                film_stat_sq.c.film_id,
                film_stat_sq.c.like,
                film_stat_sq.c.dont_like,
                rating_func.label('rating'),
            )
            .filter(watched_func)
            .order_by(desc('rating'))
        )

    def _get_film_to_view_q(self, group_id: int, user_id: int) -> Query:
        film_stat_sq = self._get_film_stat_subquery(group_id)

        # Exclude films user already watch
        exclude_films = self._db.query(FilmMarkDB.film_id).filter(
            and_(
                FilmMarkDB.user_id == user_id,
                or_(FilmMarkDB.mark == 1, FilmMarkDB.mark == 2),
            )
        )

        watched_func = or_(film_stat_sq.c.like > 0, film_stat_sq.c.dont_like > 0)
        rating_func = film_stat_sq.c.like - film_stat_sq.c.dont_like + watched_func * 2

        return (
            self._db.query(
                film_stat_sq.c.film_id,
                film_stat_sq.c.like,
                film_stat_sq.c.dont_like,
                rating_func.label('rating'),
            )
            .filter(film_stat_sq.c.film_id.notin_(exclude_films))
            .order_by(desc('rating'), func.random())
        )

    def _film_rating_to_result(
        self, films_rating: List[Tuple[int, int, int, int]], users_count: int
    ) -> List[GroupFilmWithStat]:
        films_list = self._films.get_films_by_id([f[0] for f in films_rating])
        films = {f.film_id: f for f in films_list}

        return [
            GroupFilmWithStat(
                film=films[f[0]],
                stat=GroupFilmStat(
                    **{'for': f[1]}, against=f[2], abstained=users_count - f[1] - f[2]
                ),
            )
            for f in films_rating
            if f[0] in films
        ]

    def get_top_films(
        self, group_id: int, offset: int, limit: int
    ) -> List[GroupFilmWithStat]:
        users_count = self._get_group_users_count(group_id)

        films_rating = (
            self._get_film_rating_q(group_id).offset(offset).limit(limit).all()
        )
        return self._film_rating_to_result(films_rating, users_count)

    def get_films_to_view(
        self, user_id: int, group_id: int, limit: int
    ) -> List[GroupFilmWithStat]:
        users_count = self._get_group_users_count(group_id)

        films_to_view = self._get_film_to_view_q(group_id, user_id).limit(limit).all()
        return self._film_rating_to_result(films_to_view, users_count)

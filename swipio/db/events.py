from loguru import logger

from .base import engine
from .models import SQLBase


def init_db() -> None:
    logger.info('Create database metadata')
    SQLBase.metadata.create_all(bind=engine)

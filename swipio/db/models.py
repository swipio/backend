from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import functions

from .base import SQLBase


class ParamsDB(SQLBase):
    __tablename__ = 'params'
    name = Column(String, unique=True, primary_key=True)
    value = Column(String)


class UserDB(SQLBase):
    __tablename__ = 'user'
    user_id = Column(Integer, primary_key=True)

    login = Column(String(255), unique=True)
    hashed_password = Column(String(60))
    first_name = Column(String(255))
    last_name = Column(String(255))
    disabled = Column(Boolean)
    is_admin = Column(Boolean, default=False)

    groups = relationship(
        'GroupDB', secondary='user_group', back_populates='users', uselist=True
    )

    created_at = Column(DateTime, server_default=functions.now())
    updated_at = Column(
        DateTime, server_default=functions.now(), onupdate=functions.now()
    )


class FilmDB(SQLBase):
    __tablename__ = 'film'
    film_id = Column(Integer, primary_key=True)

    title = Column(String)
    description = Column(String)
    release_year = Column(Integer)
    duration = Column(Integer)
    removed = Column(Boolean, default=False, index=True)
    preview_image_url = Column(String)

    created_at = Column(DateTime, server_default=functions.now())
    updated_at = Column(
        DateTime, server_default=functions.now(), onupdate=functions.now()
    )


class FilmMarkDB(SQLBase):
    __tablename__ = 'film_mark'
    user_id = Column(Integer, ForeignKey('user.user_id'), primary_key=True)
    film_id = Column(Integer, ForeignKey('film.film_id'), primary_key=True)
    mark = Column(Integer)
    updated_at = Column(
        DateTime, server_default=functions.now(), onupdate=functions.now()
    )


class GroupDB(SQLBase):
    __tablename__ = 'group'
    group_id = Column(Integer, primary_key=True)
    name = Column(String(255))
    description = Column(String)
    creator_id = Column(Integer, ForeignKey('user.user_id'), index=True)

    created_at = Column(DateTime, server_default=functions.now())
    updated_at = Column(
        DateTime, server_default=functions.now(), onupdate=functions.now()
    )
    users = relationship(
        'UserDB', secondary='user_group', back_populates='groups', uselist=True
    )


class UserGroupDB(SQLBase):
    __tablename__ = 'user_group'
    user_id = Column(Integer, ForeignKey('user.user_id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('group.group_id'), primary_key=True)

    created_at = Column(DateTime, server_default=functions.now())
    updated_at = Column(
        DateTime, server_default=functions.now(), onupdate=functions.now()
    )

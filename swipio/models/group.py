from typing import List

from pydantic import BaseModel, Field

from .common import DateTimeModelMixin
from .films import Film
from .users import User


class GroupUserStat(BaseModel):
    pass


class GroupUserWithStat(BaseModel):
    user: User
    stat: GroupUserStat


class GroupFilmStat(BaseModel):
    for_: int = Field(alias='for')
    against: int
    abstained: int


class GroupFilmWithStat(BaseModel):
    film: Film
    stat: GroupFilmStat


class Group(DateTimeModelMixin):
    group_id: int
    name: str
    description: str
    users: List[GroupUserWithStat]


class GroupInDB(Group):
    pass


class GroupUserWithStatInResponse(BaseModel):
    user: GroupUserWithStat


class GroupUserWithStatListInResponse(BaseModel):
    users: List[GroupUserWithStat]


class GroupFilmWithStatListInResponse(BaseModel):
    films: List[GroupFilmWithStat]


class GroupInResponse(BaseModel):
    group: Group

from datetime import datetime

from pydantic import BaseModel


class JWTData(BaseModel):
    exp: datetime
    sub: str

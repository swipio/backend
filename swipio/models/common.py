from datetime import datetime

from pydantic import BaseConfig, BaseModel


class DateTimeModelMixin(BaseModel):
    created_at: datetime
    updated_at: datetime


class FromORMModel(BaseModel):
    class Config(BaseConfig):
        orm_mode = True

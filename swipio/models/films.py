from datetime import datetime
from typing import List

from pydantic import BaseModel

from .common import FromORMModel


class Film(BaseModel):
    film_id: int
    title: str
    description: str
    release_year: int
    duration: int
    preview_image_url: str


class FilmInDB(FromORMModel, Film):
    created_at: datetime
    updated_at: datetime
    removed: bool


class FilmInResponse(BaseModel):
    film: Film


class FilmListInResponse(BaseModel):
    films: List[Film]

from pydantic import BaseModel

from .common import DateTimeModelMixin, FromORMModel


class User(DateTimeModelMixin):
    user_id: int
    login: str
    first_name: str
    last_name: str
    disabled: bool
    is_admin: bool


class UserInDB(FromORMModel, User):
    pass


class UserPermInDB(BaseModel):
    user_id: int
    disabled: bool
    is_admin: bool


class UserLoginInfoInDB(BaseModel):
    user_id: int
    login: str
    hashed_password: str
    disabled: bool


class UserTokenInResponse(BaseModel):
    access_token: str
    token_type: str


class UserInResponse(BaseModel):
    user: User

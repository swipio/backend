from datetime import datetime
from enum import Enum

from pydantic import BaseModel

from .common import FromORMModel


class FilmMarkType(Enum):
    UNKNOWN = 'unknown'
    WANT_TO_WATCH = 'like'
    DONT_WANT_TO_WATCH = 'dont_like'


class FilmMark(BaseModel):
    user_id: int
    film_id: int
    mark: FilmMarkType = FilmMarkType.UNKNOWN
    updated_at: datetime


class FilmMarkInDB(FromORMModel, FilmMark):
    pass


class FilmMarkInResponse(BaseModel):
    film_mark: FilmMark

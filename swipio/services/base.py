from typing import Optional

from sqlalchemy.orm import Session


class BaseService:

    __instance: Optional['BaseService'] = None

    @classmethod
    def init(cls, db: Session) -> None:
        pass

    @classmethod
    def get_instance(cls) -> 'BaseService':
        assert cls.__instance
        return cls.__instance

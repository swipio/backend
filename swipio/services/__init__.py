from typing import List, Type

from sqlalchemy.orm import Session

from swipio.db import SessionLocal

from .base import BaseService
from .jwt import JWTService
from .security import SecurityService

__all__ = ['BaseService', 'JWTService', 'SecurityService', 'init_services']

all_services: List[Type[BaseService]] = [JWTService, SecurityService]


def init_services() -> None:
    db: Session = SessionLocal()

    for service in all_services:
        try:
            service.init(db)
            db.commit()
        except BaseException:
            db.rollback()
            raise

    db.close()

from typing import Optional

from passlib.context import CryptContext
from sqlalchemy.orm import Session

from .base import BaseService


class SecurityService(BaseService):

    __instance: Optional['SecurityService'] = None

    _pwd_context: CryptContext

    def __init__(self) -> None:
        self._pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')

    @classmethod
    def init(cls, db: Session) -> None:
        cls.__instance = cls()

    @classmethod
    def get_instance(cls) -> 'SecurityService':
        assert cls.__instance
        return cls.__instance

    def verify_password(self, password: str, hashed_password: str) -> bool:
        return self._pwd_context.verify(password, hashed_password)

    def get_password_hash(self, password: str) -> str:
        return self._pwd_context.hash(password)

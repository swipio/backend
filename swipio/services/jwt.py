import secrets
from datetime import datetime, timedelta
from typing import Any, Dict, Optional

from jose import JWTError, jwt
from loguru import logger
from pydantic import ValidationError
from sqlalchemy.orm import Session

from swipio.db.exceptions import NotFoundRepoException
from swipio.db.repositories import ParamsRepository
from swipio.models.jwt import JWTData

from .base import BaseService


class JWTServiceException(Exception):
    pass


class BadTokenJWTServiceException(JWTServiceException):
    pass


class JWTService(BaseService):

    DEFAULT_EXPIRES_DELTA = timedelta(days=30)
    JWT_ALGORITHM = 'HS256'

    __instance: Optional['JWTService'] = None

    _jwt_secret_key: str

    def __init__(self, secret_key: str):
        self._jwt_secret_key = secret_key

    @staticmethod
    def _get_or_generate_jwt_secret_key(params_repo: ParamsRepository) -> str:
        try:
            res = params_repo.get('jwt_secret_key')
            logger.info('JWT secret key loaded')
            return res
        except NotFoundRepoException:
            res = secrets.token_hex(32)
            params_repo.set('jwt_secret_key', res)
            logger.info('New JWT secret key gererated')
            return res

    @classmethod
    def init(cls, db: Session) -> None:
        secret_key = cls._get_or_generate_jwt_secret_key(ParamsRepository(db))
        cls.__instance = cls(secret_key)

    @classmethod
    def get_instance(cls) -> 'JWTService':
        assert cls.__instance
        return cls.__instance

    def create_jwt_token(
        self,
        subject: str,
        expires_delta: timedelta,
        payload: Optional[Dict[str, Any]] = None,
    ) -> str:
        to_encode = (payload or {}).copy()
        to_encode.update(
            JWTData(sub=subject, exp=datetime.utcnow() + expires_delta).dict()
        )
        return jwt.encode(to_encode, self._jwt_secret_key, algorithm=self.JWT_ALGORITHM)

    def create_access_token_for_user(
        self, user_id: int, expires_delta: Optional[timedelta] = None
    ) -> str:
        return self.create_jwt_token(
            str(user_id), expires_delta or self.DEFAULT_EXPIRES_DELTA
        )

    def get_user_id_from_token(self, token: str) -> int:
        try:
            payload = jwt.decode(
                token, self._jwt_secret_key, algorithms=[self.JWT_ALGORITHM]
            )
        except JWTError as e:
            raise BadTokenJWTServiceException(
                'Failed for decode and verify given token'
            ) from e

        try:
            data = JWTData(**payload)
        except ValidationError as e:
            raise BadTokenJWTServiceException('Malformed payload in token') from e

        try:
            return int(data.sub)
        except ValueError as e:
            raise BadTokenJWTServiceException(
                'malformed payload in token: sub is not int'
            ) from e

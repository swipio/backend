from typing import Callable

from fastapi import FastAPI

from swipio.db import init_db
from swipio.services import init_services


def create_start_app_handler(_app: FastAPI) -> Callable[[], None]:
    def start_app() -> None:
        init_db()
        init_services()

    return start_app


def create_stop_app_handler(_app: FastAPI) -> Callable[[], None]:
    def stop_app() -> None:
        pass

    return stop_app

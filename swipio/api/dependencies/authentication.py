from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN

from swipio.db.exceptions import NotFoundRepoException
from swipio.db.repositories import UsersRepository
from swipio.models.users import UserPermInDB
from swipio.services.jwt import BadTokenJWTServiceException, JWTService

from .database import get_repository

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')


async def _get_current_user_id(token: str = Depends(oauth2_scheme)) -> int:
    jwt = JWTService.get_instance()

    try:
        return jwt.get_user_id_from_token(token)
    except BadTokenJWTServiceException as e:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail='Bad or expired token',
            headers={'WWW-Authenticate': 'Bearer'},
        ) from e


async def _get_current_user_perm(
    user_id: int = Depends(_get_current_user_id),
    user_repo: UsersRepository = Depends(get_repository(UsersRepository)),
) -> UserPermInDB:
    try:
        return user_repo.get_user_perm_by_id(user_id)
    except NotFoundRepoException as e:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail='Could not validate credentials',
            headers={'WWW-Authenticate': 'Bearer'},
        ) from e


async def get_current_active_user(
    current_user: UserPermInDB = Depends(_get_current_user_perm),
) -> UserPermInDB:
    if current_user.disabled:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail='Inactive user',
            headers={'WWW-Authenticate': 'Bearer'},
        )
    return current_user


async def get_current_admin_user(
    current_user: UserPermInDB = Depends(get_current_active_user),
) -> UserPermInDB:
    if not current_user.is_admin:
        raise HTTPException(
            status_code=HTTP_403_FORBIDDEN,
            detail='You do not have enough permissions',
        )
    return current_user

from typing import Callable, Type

from swipio.services.base import BaseService


def get_service(service_type: Type[BaseService]) -> Callable[[], BaseService]:
    def wrap() -> BaseService:
        return service_type.get_instance()

    return wrap

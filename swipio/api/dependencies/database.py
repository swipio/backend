from typing import Callable, Generator, Type

from fastapi import Depends
from sqlalchemy.orm import Session

from swipio.db import SessionLocal
from swipio.db.repositories import BaseRepository


def _get_db_session() -> Generator[Session, None, None]:
    new_session = SessionLocal()
    try:
        yield new_session
        new_session.commit()
    except Exception:
        new_session.rollback()
        raise
    finally:
        new_session.close()


def get_repository(
    repo_type: Type[BaseRepository],
) -> Callable[[Session], BaseRepository]:
    def wrap(db: Session = Depends(_get_db_session)) -> BaseRepository:
        return repo_type(db)

    return wrap

from typing import Optional

from fastapi import APIRouter, Depends, HTTPException
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_404_NOT_FOUND,
    HTTP_501_NOT_IMPLEMENTED,
)

from swipio.api.dependencies import get_current_admin_user, get_repository
from swipio.db.exceptions import NotFoundRepoException
from swipio.db.repositories import FilmsRepository
from swipio.models.films import Film, FilmInResponse, FilmListInResponse
from swipio.models.users import UserPermInDB

router = APIRouter()


@router.get(
    '/',
    response_model=FilmListInResponse,
    summary='Select films that match a given filter (admins only)',
)
def select_films(
    offset: int,
    limit: int,
    _cur_user: UserPermInDB = Depends(get_current_admin_user),
    film_repo: FilmsRepository = Depends(get_repository(FilmsRepository)),
) -> FilmListInResponse:
    res = film_repo.get_films(offset, limit)
    return FilmListInResponse(films=res)


@router.post(
    '/create',
    status_code=HTTP_201_CREATED,
    response_model=FilmInResponse,
    summary='Create new film (admins only)',
)
def create_new_film(
    title: str,
    description: str,
    release_year: int,
    duration: int,
    preview_image_url: str,
    _cur_user: UserPermInDB = Depends(get_current_admin_user),
    film_repo: FilmsRepository = Depends(get_repository(FilmsRepository)),
) -> FilmInResponse:
    film_id = film_repo.add_new_film(
        title, description, release_year, duration, preview_image_url
    )
    return FilmInResponse(film=film_repo.get_film_by_id(film_id))


@router.get(
    '/{film_id}',
    response_model=FilmInResponse,
    summary='Get info for the film (admins only)',
)
def get_film_info(
    film_id: int,
    _cur_user: UserPermInDB = Depends(get_current_admin_user),
    film_repo: FilmsRepository = Depends(get_repository(FilmsRepository)),
) -> FilmInResponse:
    try:
        film = film_repo.get_film_by_id(film_id)
    except NotFoundRepoException as e:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=f'Film with id {film_id} not found'
        ) from e
    return FilmInResponse(film=Film(**film.dict()))


@router.post(
    '/{film_id}/delete',
    status_code=HTTP_200_OK,
    summary='Mark the film as deleted (admins only)',
)
def delete_film(
    film_id: int,
    _cur_user: UserPermInDB = Depends(get_current_admin_user),
    film_repo: FilmsRepository = Depends(get_repository(FilmsRepository)),
) -> None:
    film_repo.remove_film(film_id)


@router.put(
    '/{film_id}/edit',
    status_code=HTTP_200_OK,
    summary='Edit info of the film (admins only)',
    deprecated=True,
)
def edit_film(
    film_id: int,
    title: Optional[str],
    description: Optional[str],
    release_year: Optional[int],
    duration: Optional[int],
    preview_image_url: Optional[str],
    _cur_user: UserPermInDB = Depends(get_current_admin_user),
    film_repo: FilmsRepository = Depends(get_repository(FilmsRepository)),
) -> None:
    raise HTTPException(
        status_code=HTTP_501_NOT_IMPLEMENTED, detail='API method not yet implemented'
    )

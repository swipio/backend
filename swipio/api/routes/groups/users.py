from fastapi import APIRouter, HTTPException
from starlette.status import HTTP_501_NOT_IMPLEMENTED

from swipio.models.group import (
    GroupUserWithStatInResponse,
    GroupUserWithStatListInResponse,
)

router = APIRouter()


@router.get(
    '/{group_id}/users',
    response_model=GroupUserWithStatListInResponse,
    summary='Get list of users of the group with group statistics',
    deprecated=True,
)
def get_group_users(group_id: int) -> GroupUserWithStatListInResponse:
    raise HTTPException(
        status_code=HTTP_501_NOT_IMPLEMENTED, detail='API method not yet implemented'
    )


@router.get(
    '/{group_id}/users/me',
    response_model=GroupUserWithStatInResponse,
    summary='Get the current user of the group with group statistics',
    deprecated=True,
)
def get_group_user_me(group_id: int) -> GroupUserWithStatInResponse:
    raise HTTPException(
        status_code=HTTP_501_NOT_IMPLEMENTED, detail='API method not yet implemented'
    )


@router.get(
    '/{group_id}/users/{user_id}',
    response_model=GroupUserWithStatInResponse,
    summary='Get the users of the group with group statistics',
    deprecated=True,
)
def get_group_user(group_id: int, user_id: int) -> GroupUserWithStatInResponse:
    raise HTTPException(
        status_code=HTTP_501_NOT_IMPLEMENTED, detail='API method not yet implemented'
    )


@router.post(
    '/{group_id}/users/{user_id}/exclude',
    summary='Exclude the user from the group',
    deprecated=True,
)
def exclude_user_from_group(group_id: int, user_id: int) -> None:
    raise HTTPException(
        status_code=HTTP_501_NOT_IMPLEMENTED, detail='API method not yet implemented'
    )

from typing import Optional

from fastapi import APIRouter, Depends, HTTPException
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_403_FORBIDDEN,
    HTTP_404_NOT_FOUND,
    HTTP_501_NOT_IMPLEMENTED,
)

from swipio.api.dependencies import get_current_active_user, get_repository
from swipio.db.exceptions import NotFoundRepoException
from swipio.db.repositories import GroupsRepository
from swipio.models.group import GroupFilmWithStatListInResponse, GroupInResponse
from swipio.models.users import UserPermInDB

router = APIRouter()


@router.post(
    '/create',
    status_code=HTTP_201_CREATED,
    response_model=GroupInResponse,
    summary='Create new group',
)
def create_new_group(
    name: str,
    description: str,
    cur_user: UserPermInDB = Depends(get_current_active_user),
    group_repo: GroupsRepository = Depends(get_repository(GroupsRepository)),
) -> GroupInResponse:
    group_id = group_repo.add_new_group(name, description, cur_user.user_id)
    group = group_repo.get_group_by_id(group_id)
    return GroupInResponse(group=group)


@router.get(
    '/{group_id}', response_model=GroupInResponse, summary='Get info of the group'
)
def get_group_info(
    group_id: int,
    cur_user: UserPermInDB = Depends(get_current_active_user),
    group_repo: GroupsRepository = Depends(get_repository(GroupsRepository)),
) -> GroupInResponse:
    if not group_repo.is_user_in_group(cur_user.user_id, group_id):
        raise HTTPException(
            status_code=HTTP_403_FORBIDDEN, detail='Join to group to view group info'
        )

    try:
        res = group_repo.get_group_by_id(group_id)
    except NotFoundRepoException as e:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=f'Group with id {group_id} not found'
        ) from e
    return GroupInResponse(group=res)


@router.put(
    '/{group_id}/edit',
    status_code=HTTP_200_OK,
    summary='Edit info of the group',
    deprecated=True,
)
def edit_group(
    group_id: int,
    name: Optional[str],
    desc: Optional[str],
    cur_user: UserPermInDB = Depends(get_current_active_user),
    group_repo: GroupsRepository = Depends(get_repository(GroupsRepository)),
) -> None:
    raise HTTPException(
        status_code=HTTP_501_NOT_IMPLEMENTED, detail='API method not yet implemented'
    )


@router.post('/{group_id}/join', summary='Join to the group')
def join_group(
    group_id: int,
    cur_user: UserPermInDB = Depends(get_current_active_user),
    group_repo: GroupsRepository = Depends(get_repository(GroupsRepository)),
) -> None:
    group_repo.join_to_group(cur_user.user_id, group_id)


@router.get(
    '/{group_id}/top_films',
    response_model=GroupFilmWithStatListInResponse,
    summary='Get a list of the top films to watch',
)
def get_group_top_films(
    group_id: int,
    page: int = 0,
    _cur_user: UserPermInDB = Depends(get_current_active_user),
    group_repo: GroupsRepository = Depends(get_repository(GroupsRepository)),
) -> GroupFilmWithStatListInResponse:
    res = group_repo.get_top_films(group_id, page * 50, 50)
    return GroupFilmWithStatListInResponse(films=res)


@router.get(
    '/{group_id}/films_to_view',
    response_model=GroupFilmWithStatListInResponse,
    summary='Get a list of films to view',
)
def get_group_films_to_view(
    group_id: int,
    count: int,
    cur_user: UserPermInDB = Depends(get_current_active_user),
    group_repo: GroupsRepository = Depends(get_repository(GroupsRepository)),
) -> GroupFilmWithStatListInResponse:
    res = group_repo.get_films_to_view(cur_user.user_id, group_id, count)
    return GroupFilmWithStatListInResponse(films=res)

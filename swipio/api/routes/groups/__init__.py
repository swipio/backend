from fastapi import APIRouter

from . import common, users

router = APIRouter()
router.include_router(common.router)
router.include_router(users.router)

from typing import Optional

from fastapi import APIRouter, Depends, HTTPException
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_404_NOT_FOUND,
    HTTP_409_CONFLICT,
    HTTP_501_NOT_IMPLEMENTED,
)

from swipio.api.dependencies import get_current_active_user, get_repository, get_service
from swipio.db.exceptions import AlreadyExistRepoException, NotFoundRepoException
from swipio.db.repositories import UsersRepository
from swipio.models.users import UserInResponse, UserPermInDB
from swipio.services import SecurityService

router = APIRouter()


@router.post(
    '/register',
    status_code=HTTP_201_CREATED,
    response_model=UserInResponse,
    summary='Register new user',
)
def register_new_user(
    username: str,
    password: str,
    first_name: str,
    last_name: str,
    user_repo: UsersRepository = Depends(get_repository(UsersRepository)),
    security: SecurityService = Depends(get_service(SecurityService)),
) -> UserInResponse:
    try:
        user_id = user_repo.create_user(
            username, security.get_password_hash(password), first_name, last_name
        )
    except AlreadyExistRepoException as e:
        raise HTTPException(
            status_code=HTTP_409_CONFLICT,
            detail='User with this login already exists',
            headers={'WWW-Authenticate': 'Bearer'},
        ) from e

    return UserInResponse(user=user_repo.get_user_by_id(user_id))


def _get_user_info(user_id: int, user_repo: UsersRepository) -> UserInResponse:
    try:
        return UserInResponse(user=user_repo.get_user_by_id(user_id))
    except NotFoundRepoException as e:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=f'User with id {user_id} not found'
        ) from e


@router.get(
    '/me', response_model=UserInResponse, summary='Get info for the current user'
)
def get_user_info_me(
    cur_user: UserPermInDB = Depends(get_current_active_user),
    user_repo: UsersRepository = Depends(get_repository(UsersRepository)),
) -> UserInResponse:
    return _get_user_info(cur_user.user_id, user_repo)


@router.get(
    '/{user_id}', response_model=UserInResponse, summary='Get info for the user'
)
def get_user_info(
    user_id: int,
    _cur_user: UserPermInDB = Depends(get_current_active_user),
    user_repo: UsersRepository = Depends(get_repository(UsersRepository)),
) -> UserInResponse:
    return _get_user_info(user_id, user_repo)


@router.put(
    '/me/edit',
    status_code=HTTP_200_OK,
    summary='Edit info of current user',
    deprecated=True,
)
def edit_user(
    username: str,
    password: Optional[str],
    first_name: Optional[str],
    last_name: Optional[str],
    cur_user_id: int = Depends(get_current_active_user),
) -> None:
    raise HTTPException(
        status_code=HTTP_501_NOT_IMPLEMENTED, detail='API method not yet implemented'
    )

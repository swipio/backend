from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from starlette.status import HTTP_401_UNAUTHORIZED

from swipio.api.dependencies import get_repository, get_service
from swipio.db.exceptions import NotFoundRepoException
from swipio.db.repositories import UsersRepository
from swipio.models.users import UserTokenInResponse
from swipio.services import JWTService, SecurityService

router = APIRouter()


@router.post(
    '/token',
    response_model=UserTokenInResponse,
    summary='Login for access token using OAuth2',
)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    user_repo: UsersRepository = Depends(get_repository(UsersRepository)),
    jwt: JWTService = Depends(get_service(JWTService)),
    security: SecurityService = Depends(get_service(SecurityService)),
) -> UserTokenInResponse:

    try:
        user_login_data = user_repo.get_user_login_data(form_data.username)
    except NotFoundRepoException as e:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'},
        ) from e

    if not security.verify_password(
        form_data.password, user_login_data.hashed_password
    ):
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'},
        )

    if user_login_data.disabled:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail='Inactive user',
            headers={'WWW-Authenticate': 'Bearer'},
        )

    access_token = jwt.create_access_token_for_user(user_login_data.user_id)
    return UserTokenInResponse(access_token=access_token, token_type='bearer')

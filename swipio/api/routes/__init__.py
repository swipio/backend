from fastapi import APIRouter

from . import authentication, film_marks, films, groups, users

router = APIRouter()
router.include_router(authentication.router, tags=['authentication'])
router.include_router(users.router, tags=['users'], prefix='/api/v1/users')
router.include_router(films.router, tags=['films'], prefix='/api/v1/films')
router.include_router(groups.router, tags=['groups'], prefix='/api/v1/groups')
router.include_router(
    film_marks.router, tags=['film_marks'], prefix='/api/v1/film_marks'
)

from fastapi import APIRouter, Depends
from starlette.status import HTTP_200_OK

from swipio.api.dependencies import get_current_active_user, get_repository
from swipio.db.repositories import FilmMarksRepository
from swipio.models.film_marks import FilmMarkInResponse, FilmMarkType
from swipio.models.users import UserPermInDB

router = APIRouter()


@router.get(
    '/{film_id}',
    response_model=FilmMarkInResponse,
    summary="Get mark (like/don't like) of the film",
)
def get_film_mark(
    film_id: int,
    cur_user: UserPermInDB = Depends(get_current_active_user),
    film_mark_repo: FilmMarksRepository = Depends(get_repository(FilmMarksRepository)),
) -> FilmMarkInResponse:
    film_mark = film_mark_repo.get_mark(cur_user.user_id, film_id)
    return FilmMarkInResponse(film_mark=film_mark)


@router.put(
    '/{film_id}/edit',
    status_code=HTTP_200_OK,
    summary="Edit mark (like/don't like) of the film",
)
def edit_film_mark(
    film_id: int,
    mark: FilmMarkType,
    cur_user: UserPermInDB = Depends(get_current_active_user),
    film_mark_repo: FilmMarksRepository = Depends(get_repository(FilmMarksRepository)),
) -> None:
    film_mark_repo.edit_mark(cur_user.user_id, film_id, mark)

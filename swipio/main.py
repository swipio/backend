from fastapi import FastAPI

from .api import router
from .core.events import create_start_app_handler, create_stop_app_handler


def get_application() -> FastAPI:
    tags_metadata = [
        {
            'name': 'authentication',
            'description': 'API for authentication',
        },
        {
            'name': 'users',
            'description': 'API for operations with users: registration, get user info, edit user',
        },
        {
            'name': 'films',
            'description': 'API for operations with films: create, search, get and edit',
        },
        {
            'name': 'film_marks',
            'description': "API for operations with film marks (like/don't like): get, set",
        },
        {
            'name': 'groups',
            'description': 'API for operations with groups: create, get, join, edit and get films',
        },
    ]

    res = FastAPI(title='SwipioAPI', version='0.2.0', openapi_tags=tags_metadata)
    res.add_event_handler('startup', create_start_app_handler(res))
    res.add_event_handler('shutdown', create_stop_app_handler(res))

    res.include_router(router)
    return res


app = get_application()

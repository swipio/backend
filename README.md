<div align="center">
  <img src=".gitlab/logo.svg" height="124px"/><br/>
  <h1> Swipio backend </h1>
  <p>Backend with API access for the <a href="https://gitlab.com/swipio">swipio project</a></p>
</div>



## 📝 About The Project
In a large group of friends, it is always difficult to choose a movie to watch that will suit everyone. This project aims to solve this problem. A group of friends individually scrolls through the list of available movies and uses swipe gestures to determine which movies they like. After that, the system gives out a list of films that can be watched, in order of rating (at the very top is the film that more friends liked)

## ⚡️ Quick start

First of all, make sure you have [installed](https://www.python.org/downloads/) **Python**. Version `3.6` or higher is required.

Download the repository and change the current directory:
```bash
git clone https://gitlab.com/swipio/backend.git && cd backend
```

Configure virtual environment. Make sure a `.venv` folder has been created after this step.
```bash
make venv
```

Run the project:
```bash
make up
```

Open the website `http://localhost:8000/docs` to view the API documentation

### :whale: Docker-way to quick start

Create the storage folder:

```bash
mkdir storage
```

Run the container:

```bash
docker run --rm -it \
  -v "${PWD}/storage:/opt/app/storage" \
  -p 8000:80 \
  registry.gitlab.com/swipio/backend:latest
```

Open the website `http://localhost:8000/docs` to view the API documentation

## :book: API documentation

There are 5 groups of endpoints: 

* **authentication** - API for authentication
* **users** - API for operations with users: registration, get user info, edit user
* **films** - API for operations with films: create, search, get and edit (for admins only)
* **film_marks** - API for operations with film marks (like/don't like): get, set
* **groups** - API for operations with groups: create, get, join, edit and get films

You can find more information about the API on the website `http://localhost:8000/docs` (you need to run the project locally)

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i> <br> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>v.markov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>

## :scroll: License

`Swipio backend` is free and open-source software licensed under the [Apache 2.0 License](LICENSE)

